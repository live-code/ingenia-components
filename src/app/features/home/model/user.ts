
export interface User {
  id?: number;
  name?: string;
  phone?: string;
  website?: string;
  email?: string;
}
