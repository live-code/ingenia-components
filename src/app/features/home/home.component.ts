import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { UsersService } from './services/users.service';
import { User } from './model/user';

@Component({
  selector: 'app-home',
  template: `
    <i class="fa fa-plus fa-2x" (click)="isModalOpened = true">ADD</i>
    <hr>
    <app-card 
      title="User Form" 
      icon="fa fa-times"
      [isModal]="true"
      (iconClick)="isModalOpened = false"
      *ngIf="isModalOpened"
    >
      <app-users-form
        [total]="userService.users?.length"
        [activeUser]="userService.activeUser"
        (save)="saveHandler($event)"
      ></app-users-form>
    </app-card>

    <app-card
      title="User List"
      [toggable]="true"
    >
        <app-users-list
          [users]="userService.users"
          [activeUser]="userService.activeUser"
          (select)="selectHandler($event)"
          (remove)="userService.deleteHandler($event)"
        ></app-users-list>
    </app-card>
  `,
})
export class HomeComponent  {
  isModalOpened = false;
  input: FormControl = new FormControl();

  constructor(public userService: UsersService) {
    this.userService.init()
  }

  addHandler() {
    this.userService.saveHandler({ name: this.input.value})
  }

  selectHandler(user: User) {
    this.isModalOpened = true;
    this.userService.selectUser(user);
  }

  saveHandler($event: Partial<User>) {
    this.isModalOpened = false;
    this.userService.saveHandler($event)
  }
}
