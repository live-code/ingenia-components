import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class UsersService {
  users: User[];
  activeUser: User;

  constructor(private http: HttpClient) { }

  init(): void {
    this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe(res => this.users = res )
  }

  saveHandler(user: Pick<User, 'name'>): void {
    if (this.activeUser) {
      // edit
     this.editHandler(user);
    } else {
      this.addHandler(user);
    }
  }

  addHandler(user: Pick<User, 'name'>): void {
    this.http.post<User>('https://jsonplaceholder.typicode.com/users', user)
      .subscribe(res => this.users = [...this.users, res])
  }

  deleteHandler(id: number): void {
    this.http.delete(`https://jsonplaceholder.typicode.com/users/${id}`)
      .subscribe(res => {
        this.users = this.users.filter(u => u.id !== id)
      })
  }

  selectUser(user: User) {
    this.activeUser = user;
  }

  editHandler(user: Pick<User, 'name'>): void {
    this.http.patch<User>(`https://jsonplaceholder.typicode.com/users/${this.activeUser.id}`, user)
      .subscribe(res => {
        this.users = this.users.map(u => {
          return u.id === this.activeUser.id ? res : u;
        })
      })
  }
}
