import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersFormComponent } from './components/users-form.component';
import { UsersListComponent } from './components/users-list.component';
import { UsersFiltersComponent } from './components/users-filters.component';
import { UsersListItemComponent } from './components/users-list-item.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [HomeComponent, UsersFormComponent, UsersListComponent, UsersFiltersComponent, UsersListItemComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class HomeModule { }
