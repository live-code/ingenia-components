import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../model/user';

@Component({
  selector: 'app-users-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <!--user list-->
    <app-users-list-item
      *ngFor="let user of users"
      [user]="user"
      [selected]="user.id === activeUser?.id"
      (select)="select.emit(user)"
      (remove)="remove.emit(user.id)"
    >
    </app-users-list-item>
  `,
})
export class UsersListComponent {
  @Input() users: User[];
  @Input() activeUser: User;
  @Output() remove: EventEmitter<number> = new EventEmitter<number>();
  @Output() select: EventEmitter<User> = new EventEmitter<User>();

}
