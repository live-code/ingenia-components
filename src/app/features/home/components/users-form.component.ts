import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';
import { User } from '../model/user';

@Component({
  selector: 'app-users-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <!--<pre>{{activeUser | json}}</pre>-->
    <input type="text" [formControl]="input">
    <button (click)="save.emit({ name: input.value})">save</button>
    <pre>{{total}} users</pre>
  `,
})
export class UsersFormComponent {
  @Output() save: EventEmitter<Partial<User>> = new EventEmitter<Partial<User>>();
  @Input() activeUser: User;
  @Input() total: number;

  input: FormControl = new FormControl();

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes)
    if (changes.activeUser && changes.activeUser.currentValue) {
      // this.input.setValue(this.activeUser.name);
      this.input.setValue(changes.activeUser.currentValue.name);
    }
  }
}
