import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '../model/user';

@Component({
  selector: 'app-users-list-item',
  template: `
    <li
      [ngClass]="{active: selected}"
      (click)="toggleVisibility()">
      {{user.name}}
      <div *ngIf="opened">
        {{user.phone}}
        {{user.website}}
        {{user.email}}
      </div>
      <button (click)="selectUser($event)">select</button>
      <button (click)="removeHandler($event)">del</button>
    </li>
  `,
  styles: [`
    .active { background-color: orange}
  `]
})
export class UsersListItemComponent {
  @Input() user: User;
  @Input() selected: boolean;
  @Output() select: EventEmitter<void> = new EventEmitter<void>();
  @Output() remove: EventEmitter<void> = new EventEmitter<void>();

  opened = false;

  toggleVisibility() {
    this.opened = !this.opened
  }

  selectUser(event: MouseEvent) {
    event.stopPropagation();
    this.select.emit()
  }
  removeHandler(event: MouseEvent) {
    event.stopPropagation();
    this.remove.emit()
  }
}
