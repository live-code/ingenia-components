import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
   
    <button routerLink="home">Home</button>
    <button routerLink="catalog">catalog</button>
    <hr>
    <router-outlet></router-outlet>
    
    
    
    
  `,
})
export class AppComponent {

}
