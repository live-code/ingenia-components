import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

/**
 * This is a card that can act like a modal and can be toggable
 */
@Component({
  selector: 'app-card',
  template: `
    <div class="card" [ngClass]="{myModal: isModal}">
      <div class="card-header" (click)="toggle()">
        {{title}}
        <i class="pull-right"
           (click)="iconClick.emit()"
           [ngClass]="icon" *ngIf="icon"></i>
      </div>
      <div class="card-body" *ngIf="isOpened">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: [`    
    .myModal { 
      position: fixed;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      background-color: white;
      z-index: 10;
    }
  `]
})
export class CardComponent {
  /**
   * Header Icon
   */
  @Input() icon: string;
  /**
   * if card acts like a modal
   */
  @Input() isModal: boolean;
  /**
   * if card can be toggable
   */
  @Input() toggable: boolean;
  /**
   * title in header
   */
  @Input() title: string;
  /**
   * Dispatch when icon is clicked
   */
  @Output() iconClick: EventEmitter<void> = new EventEmitter<void>()

  /**
   * Toggable status
   */
  isOpened = true;

  /**
   * Toggle the body
   */
  toggle() {
    if (this.toggable) {
      this.isOpened = !this.isOpened
    }
  }
}
