import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardModule } from './components/card.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CardModule
  ],
  exports: [CardModule],
})
export class SharedModule { }
